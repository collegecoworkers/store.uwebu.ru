<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "product_order".
 *
 * @property integer $id
 * @property integer $product_id
 * @property integer $order_id
 *
 * @property Order $order
 * @property Product $product
 */
class ProductOrder extends \yii\db\ActiveRecord
{
	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return 'product_order';
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['product_id', 'order_id'], 'integer'],
			[['order_id'], 'exist', 'skipOnError' => true, 'targetClass' => Order::className(), 'targetAttribute' => ['order_id' => 'id']],
			[['product_id'], 'exist', 'skipOnError' => true, 'targetClass' => Product::className(), 'targetAttribute' => ['product_id' => 'id']],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id' => 'ID',
			'product_id' => 'Product ID',
			'order_id' => 'Order ID',
		];
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getOrder()
	{
		return $this->hasOne(Order::className(), ['id' => 'order_id']);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getProduct()
	{
		return $this->hasOne(Product::className(), ['id' => 'product_id']);
	}
}
