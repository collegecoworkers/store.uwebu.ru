<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "order".
 *
 * @property integer $id
 * @property string $title
 *
 * @property ProductOrder[] $productOrders
 */
class Order extends \yii\db\ActiveRecord
{
	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return 'order';
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id' => 'ID',
			'product_id' => 'Продукт',
			'email' => 'Email',
			'phone' => 'Телефон',
			'status' => 'Статус',
			'date' => 'Дата',
		];
	}

	public function getProducts()
	{
		return $this->hasMany(Product::className(), ['id' => 'product_id'])
			->viaTable('product_order', ['order_id' => 'id']);
	}

	public function getByProduct($product_id)
	{
		return Order::find()->where(['product_id' => $product_id])->all();
	}

	public function getStatus(){
		switch ($this->status) {
			case 1:
				return 'готов';
			case 2:
				return 'отменен';
			default:
				return 'в ожидании';
		}
	}

	public function getProduct(){
		return Product::find()->where(['id' => $this->product_id])->one();
	}

}
