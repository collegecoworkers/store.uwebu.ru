<?php

namespace app\models;

use Yii;
use yii\data\Pagination;

/**
 * This is the model class for table "category".
 *
 * @property integer $id
 * @property string $title
 */
class Category extends \yii\db\ActiveRecord
{
	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return 'category';
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['title'], 'string', 'max' => 255],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id' => 'ID',
			'title' => 'Title',
		];
	}

	public function getProducts()
	{
		return $this->hasMany(Product::className(), ['category_id' => 'id']);
	}

	public function getProductsCount()
	{
		return $this->getProducts()->count();
	}
	
	public static function getAll($param = null)
	{
		if($param){
			// die;
			return Category::find()->where($param)->all();
		} else{
			return Category::find()->all();
		}
	}
	
	public static function getProductsByCategory($id)
	{
		// build a DB query to get all products
		$query = Product::find()->where(['category_id'=>$id]);

		// get the total number of products (but do not fetch the product data yet)
		$count = $query->count();

		// create a pagination object with the total count
		$pagination = new Pagination(['totalCount' => $count, 'pageSize'=>6]);

		// limit the query using the pagination and retrieve the products
		$products = $query->offset($pagination->offset)
			->limit($pagination->limit)
			->all();

		$data['products'] = $products;
		$data['pagination'] = $pagination;
		
		return $data;
	}
}
