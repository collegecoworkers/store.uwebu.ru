<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

if(isset($is_single) && $is_single){
	$procent = floor($project->collected_sum/$project->need_sum * 100) . '%';
}
?>
<div class="sidebar">
	<?php if(isset($is_single) && $is_single): ?>
		<aside>
		<div class="sidebar_title_1">Собранно</div>
			<div class="block_sidebar_latest_reviews">
				<div class="review">
					<div class="title"><?= $project->collected_sum ?> / <?= $project->need_sum ?></div>
					<div class="num"><?= $procent ?></div>
					<div class="progress"><div class="bar" style="width:<?= ($procent <= 100) ? $procent : '100%; background-color: rgb(51, 204, 205);' ?>"></div></div>
				</div>
			</div>
		</aside>
		
		<?php if(!Yii::$app->user->isGuest): ?>
			<aside>
				<div class="sidebar_title_1">Перевести деньги</div>
				<div class="block_leave_comment_1" style="padding-top: 0;">
					<div class="form">
						<?php $form = ActiveForm::begin(); ?>
							<div class="fields" style="float: none;">
								<div class="label">Сумма</div>
								<div class="field">
									<?= $form->field($payment, 'sum')->textInput(['class'=>'w_focus_mark required'])->label(false) ?>
								</div>
							</div>

							<?= Html::submitButton('<p class="general_button_type_3">Отправить</p>', ['class' => 'btn-pay' ]) ?>
						<?php ActiveForm::end(); ?>
					</div>
				</div>
			</aside>
		<?php else: ?>
			<aside>
				<div class="sidebar_title_1">Перевести деньги</div>
				<div class="block_leave_comment_1" style="padding-top: 0;">
					<p >Чтобы переводить деньги нужно авторизоваться</p>
					<a href="<?= Url::toRoute(['auth/login'])?>"><p class="general_button_type_3">Войти</p></a>
				</div>
			</aside>
		<?php endif ?>
	<?php endif ?>
	<aside>
		<div class="sidebar_title_1">Популярные посты</div>
		
		<div class="block_sidebar_popular_posts">
			<?php foreach($popular as $project):?>
				<article>
					<div class="image"><a href="<?= Url::toRoute(['site/view','id'=>$project->id]);?>"><img src="<?= $project->getImage();?>" width=86 height=66 style="object-fit: contain;" alt=""></a></div>
					
					<div class="content">
						<div class="title">
							<a href="<?= Url::toRoute(['site/view','id'=>$project->id]);?>"><?= $project->title?></a>
						</div>
						
						<div class="info">
							<div class="date"><?= $project->getDate();?></div>
						</div>
					</div>
				</article>
			<?php endforeach;?>
		</div>
	</aside>

	<aside>
		<div class="sidebar_title_1">Последние посты</div>
		
		<div class="block_sidebar_popular_posts">

			<?php foreach($recent as $project):?>
				<article>
					<div class="image"><a href="<?= Url::toRoute(['site/view','id'=>$project->id]);?>"><img src="<?= $project->getImage();?>" width=86 height=66 style="object-fit: contain;" alt=""></a></div>
					
					<div class="content">
						<div class="title">
							<a href="<?= Url::toRoute(['site/view','id'=>$project->id]);?>"><?= $project->title?></a>
						</div>
						
						<div class="info">
							<div class="date"><?= $project->getDate();?></div>
						</div>
					</div>
				</article>
			<?php endforeach;?>

		</div>
	</aside>
	<aside>
	<div class="sidebar_title_1">Категории</div>
		<div class="block_sidebar_most_commented">
			<?php foreach($categories as $category):?>
				<article>
					<a href="<?= Url::toRoute(['site/category','id'=>$category->id]);?>">
						<span class="num"><?= $category->getProjectsCount();?><span class="tail"></span></span>
						<span class="title"><?= $category->title?></span>
					</a>
				</article>
			<?php endforeach;?>

		</div>
	</aside>
</div>
