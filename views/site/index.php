<?php
use yii\helpers\Url;
use yii\widgets\LinkPager;
?>

<header>
	<section class="bottom">
		<div class="inner">
			<div class="block_secondary_menu">
				<nav>
					<ul>
						<?php foreach($categories as $category):?>
							<li><a href="<?= Url::toRoute(['site/category','id'=>$category->id]);?>"><?= $category->title?></a></li>
						<?php endforeach;?>
					</ul>
				</nav>
			</div>
		</div>
	</section>
</header>


<!-- CONTENT BEGIN -->
<div id="content" class="sidebar_right">
	<div class="inner">
		<div class="block_slider_type_1 general_not_loaded">
			<div id="slider" class="slider flexslider">
				<ul class="slides">
					<?php foreach($popular as $project):?>
						<li>
							<img src="<?= $project->getImage();?>" width=940 height=403 alt="">
							<div class="animated_item text_1_1" data-animation-show="fadeInUp" data-animation-hide="fadeOutDown"><?= $project->category->title ?></div>
							<div class="animated_item text_1_2" data-animation-show="fadeInUp" data-animation-hide="fadeOutDown"><?= $project->title ?></div>
							<div class="animated_item text_1_3" data-animation-show="fadeInUp" data-animation-hide="fadeOutDown"><a href="<?= Url::toRoute(['site/view','id'=>$project->id]);?>" class="general_button_type_1">Читать</a></div>
						</li>
					<?php endforeach;?>
				</ul>
			</div>
			
			<script type="text/javascript">
				jQuery(function() {
					init_slider_1('#slider');
				});
			</script>
		</div>

		<div class="block_general_title_1 w_margin_1"></div>

		<div class="main_content">
			<div class="block_posts type_3">

		<?= $this->render('/partials/item', [
			'projects'=>$projects
		]);?>

			</div>
			
			<div class="separator" style="height:43px;"></div>
			
			<div class="block_pager_1">
				<?php
					echo LinkPager::widget([
						'pagination' => $pagination,
					]);
				?>
				<div class="clearboth"></div>
			</div>
		</div>

		<?= $this->render('/partials/sidebar', [
			'popular'=>$popular,
			'recent'=>$recent,
			'categories'=>$categories
		]);?>

		<div class="clearboth"></div>
	</div>
</div>
<!-- CONTENT END -->