<?php

namespace app\modules\admin\controllers;

use Yii;
use app\models\Order;
use app\models\OrderSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * OrderController implements the CRUD actions for Order model.
 */
class OrderController extends Controller
{
	/**
	 * @inheritdoc
	 */
	public function behaviors()
	{
		return [
			'verbs' => [
				'class' => VerbFilter::className(),
				'actions' => [
					'delete' => ['POST'],
				],
			],
		];
	}

	/**
	 * Lists all Order models.
	 * @return mixed
	 */
	public function actionIndex()
	{
		$searchModel = new OrderSearch();
		$dataProvider = $searchModel->search(Yii::$app->request->queryParams);

		return $this->render('index', [
			'searchModel' => $searchModel,
			'dataProvider' => $dataProvider,
		]);
	}

	public function actionDelete($id)
	{
		$this->findModel($id)->delete();

		return $this->redirect(['index']);
	}

	public function actionUpdate($id)
	{
		$model = $this->findModel($id);
		
		if ($model->load(Yii::$app->request->post())) {
			$model->email = Yii::$app->request->post()['Order']['email'];
			$model->phone = Yii::$app->request->post()['Order']['phone'];
			$model->status = Yii::$app->request->post()['Order']['status'];
			$model->save();
			return $this->redirect(['index']);
		} else {

			$statuses = [];
			$statuses[0] = 'в ожидании';
			$statuses[1] = 'готов';
			$statuses[2] = 'отменен';

			return $this->render('update', [
				'model' => $model,
				'statuses' => $statuses,
			]);
		}
	}

	protected function findModel($id)
	{
		if (($model = Order::findOne($id)) !== null) {
			return $model;
		} else {
			throw new NotFoundHttpException('The requested page does not exist.');
		}
	}

}
