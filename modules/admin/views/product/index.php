<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ProjectSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Товары';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="article-index">

	<h1><?= Html::encode($this->title) ?></h1>

	<p>
		<?= Html::a('Создать товар', ['create'], ['class' => 'btn btn-success']) ?>
	</p>
	<?= GridView::widget([
		'dataProvider' => $dataProvider,
		'filterModel' => $searchModel,
		'columns' => [
			'id',
			'title',
			'price',
			[
				'label' => 'Заказов',
				'value' => function($data){
					return count($data->getOrders());
				}
			],
			[
				'attribute' => 'category_id',
				'value' => function($data){
					return $data->getCategory()->title;
				}
			],
			['class' => 'yii\grid\ActionColumn'],
		],
	]); ?>
</div>
