<?php

namespace app\controllers;

use app\models\Project;
use app\models\Category;
// use app\models\CommentForm;
use Yii;
use yii\data\Pagination;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\Payment;

class SiteController extends Controller {

	public function behaviors() {
		return [
		];
	}

	public function actions() {
		return [
			'error' => [
				'class' => 'yii\web\ErrorAction',
			],
			'captcha' => [
				'class' => 'yii\captcha\CaptchaAction',
				'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
			],
		];
	}

	public function actionIndex() {
		if (Yii::$app->user->isGuest) {
			return $this->redirect('/web/auth/login');
		} else {
			return $this->redirect('/web/admin/default/index');
		}
	}

}
