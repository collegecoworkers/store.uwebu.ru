<?php

use yii\db\Migration;

/**
 * Handles the creation of table `project_payment`.
 */
class m170124_021633_create_project_payment_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('project_payment', [
            'id' => $this->primaryKey(),
            'project_id'=>$this->integer(),
            'payment_id'=>$this->integer()
        ]);

        // creates index for column `user_id`
        $this->createIndex(
            'payment_project_project_id',
            'project_payment',
            'project_id'
        );


        // add foreign key for table `user`
        $this->addForeignKey(
            'payment_project_project_id',
            'project_payment',
            'project_id',
            'project',
            'id',
            'CASCADE'
        );

        // creates index for column `user_id`
        $this->createIndex(
            'idx_payment_id',
            'project_payment',
            'payment_id'
        );


        // add foreign key for table `user`
        $this->addForeignKey(
            'fk-payment_id',
            'project_payment',
            'payment_id',
            'payment',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('project_payment');
    }
}
