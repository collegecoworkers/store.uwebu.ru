<?php

use yii\db\Migration;
// error 
// don't migrate
/**
 * Handles the creation of table `product`.
 */
class m170124_021553_create_product_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('product', [
            'id' => $this->primaryKey(),
            'title'=>$this->string(),
            'description'=>$this->text(),
            'content'=>$this->text(),
            'date'=>$this->date(),
            'start_date'=>$this->date(),
            'end_date'=>$this->date(),
            'need_sum'=>$this->integer(),
            'collected_sum'=>$this->integer(),
            'image'=>$this->string(),
            'viewed'=>$this->integer(),
            'user_id'=>$this->integer(),
            'status'=>$this->integer(),
            'category_id'=>$this->integer(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('product');
    }
}
